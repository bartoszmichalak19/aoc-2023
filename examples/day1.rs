use aoc_2023::read_file;
use aho_corasick::AhoCorasick;

fn main() {
    let input = read_file("inputs/in_day1.txt");

    {
        let calibration_lines: Vec<&str> = input.lines().collect();

        let calibration_values: Vec<u32> = calibration_lines
            .into_iter()
            .map(|line| line.chars()// to vec<vec<char>>
                .filter(|character| character.is_digit(10))
                .collect()) // filter only digits
            .map(|characters: Vec<char>| characters
                .into_iter()
                .map(|character| character.to_digit(10).unwrap()) // map chars to integers
                .collect())// collect to vec<u32>
            .map(|calibration_numbers: Vec<u32>| { //collect vec<u32> of calibration values
                let last_number: u32 = calibration_numbers[calibration_numbers.len() - 1];
                let first_number: u32 = calibration_numbers[0];
                let calibration_value = first_number * 10 + last_number;
                return calibration_value;
            })
            .collect();

        // find sum of calibration values
        let calibration_values_sum: u32 = calibration_values.iter().sum();
        println!(
            "PART1:\n   Sum of calibration values is {}",
            calibration_values_sum
        );
    }

    {
        ///////
        /*        PART2       */
        let nums = [
            "one", "1", "two", "2", "three", "3", "four", "4", "five", "5", "six", "6", "seven",
            "7", "eight", "8", "nine", "9",
        ];
        let mut calibration_values_sum = 0;

        let ac = AhoCorasick::new(nums).unwrap();

        for line in input.lines() {
            let matches = ac.find_overlapping_iter(line).collect::<Vec<_>>();

            let first_idx = matches.first().unwrap().pattern().as_usize();
            let last_idx = matches.last().unwrap().pattern().as_usize();

            let first_number = index_to_number(nums, first_idx);
            let last_number = index_to_number(nums, last_idx);

            calibration_values_sum += 10 * first_number + last_number;
        }

        println!(
            "PART2:\n   Sum of calibration values is {}",
            calibration_values_sum
        );
    }
}

fn word_digit_to_u32(word: &str) -> Option<u32> {
    let digit = match word {
        "zero" => Some(0),
        "one" => Some(1),
        "two" => Some(2),
        "three" => Some(3),
        "four" => Some(4),
        "five" => Some(5),
        "six" => Some(6),
        "seven" => Some(7),
        "eight" => Some(8),
        "nine" => Some(9),
        _ => None,
    };
    digit
}

fn index_to_number(nums: [&str; 18], last_idx: usize) -> u32 {
    if nums[last_idx].chars().next().unwrap().is_digit(10) {
        nums[last_idx].parse::<u32>().unwrap()
    } else {
        word_digit_to_u32(nums[last_idx]).unwrap()
    }
}
