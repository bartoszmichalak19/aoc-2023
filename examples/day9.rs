use aoc_2023::read_file;
use nalgebra::{DMatrix};

fn main() {
    let input = read_file("inputs/in_day9.txt");
    let lines: Vec<&str> = input.lines().collect();
    let sequences: Vec<Vec<i32>> = lines.into_iter().map(|line| line_to_sequence(line)).collect();
    let next_extrapolations_sum: i32 = sequences.iter().map(|sequence| extrapolate_next_value(sequence.clone())).sum();
    println!(
        "PART1:\n  Sum of extrapolations is {}",
        next_extrapolations_sum
    );

    let previous_extrapolations_sum: i32 = sequences.iter().map(|sequence| extrapolate_previous_value(sequence.clone())).sum();
    println!(
        "PART1:\n  Sum of extrapolations is {}",
        previous_extrapolations_sum
    );
}

fn line_to_sequence(line: &str) -> Vec<i32> {
    let split_nums_str: Vec<&str> = line.split_whitespace().collect();
    let sequence: Vec<i32> = split_nums_str.into_iter().map(|num_str| num_str.parse::<i32>().unwrap()).collect();
    return sequence;
}

fn adjacent_numbers_diffs(sequence: &Vec<i32>) -> Vec<i32> {
    return sequence.windows(2)
        .map(|window| window[1] - window[0]).collect();
}

fn extrapolate_previous_value(sequence: Vec<i32>) -> i32 {
    let mut matrix: Vec<Vec<i32>> = vec![sequence];

    while !matrix.last().unwrap().iter().all(|&x| x == matrix.last().unwrap()[0]) {
        matrix.push(adjacent_numbers_diffs(matrix.last().unwrap()));
    }

    for idx in (1..matrix.len()).rev() {
        let first_elem = matrix[idx].first().cloned().unwrap();
        let next_row_first_elem = matrix[idx - 1].first().cloned().unwrap();

        matrix[idx - 1].insert(0,next_row_first_elem - first_elem);
    }

    return matrix[0].first().cloned().unwrap();
}

fn extrapolate_next_value(sequence: Vec<i32>) -> i32 {
    let mut matrix: Vec<Vec<i32>> = vec![sequence];

    while !matrix.last().unwrap().iter().all(|&x| x == matrix.last().unwrap()[0]) {
        matrix.push(adjacent_numbers_diffs(matrix.last().unwrap()));
    }

    for idx in (1..matrix.len()).rev() {
        let last_elem = matrix[idx].last().cloned().unwrap();
        let next_row_last_elem = matrix[idx - 1].last().cloned().unwrap();

        matrix[idx - 1].push(next_row_last_elem + last_elem);
    }

    return matrix[0].last().cloned().unwrap();
}


fn fit_polynomial(samples: &[(usize, usize)], degree: usize) -> Option<Vec<f64>> {
    let mut matrix_a = DMatrix::zeros(samples.len(), degree + 1);
    let mut vector_b = DMatrix::zeros(samples.len(), 1);

    for (row, &(x, y)) in samples.iter().enumerate() {
        for col in 0..=degree {
            matrix_a[(row, col)] = (x as f64).powi(col as i32);
        }
        vector_b[(row, 0)] = y as f64;
    }

    // Use the pseudo_inverse method for non-square systems
    let coefficients = matrix_a.pseudo_inverse(1.0e-9);

    match coefficients {
        Ok(pseudo_inverse) => {
            let result = pseudo_inverse * vector_b;
            Some(result.iter().map(|&c| c as f64).collect())
        }
        Err(_) => None,
    }
}

fn evaluate_polynomial(coefficients: &[f64], x: usize) -> f64 {
    coefficients
        .iter()
        .enumerate()
        .map(|(exp, &coeff)| coeff * (x as f64).powi(exp as i32))
        .sum()
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn polynomial_fitting3() {
        // Example: Fit a quadratic polynomial to the provided samples
        let samples = vec![(1, 10), (2, 13), (3, 16), (4, 21), (5, 30), (6, 45)];

        let mut result = 0.0;
        if let Some(coefficients) = fit_polynomial(&samples, 3) {
            result = evaluate_polynomial(&coefficients, 7).round();
        } else {
            println!("Failed to fit polynomial");
        }
        assert_eq!(68.0, result);
    }

    #[test]
    fn polynomial_fitting2() {
        // Example: Fit a quadratic polynomial to the provided samples
        let samples = vec![(1, 1), (2, 3), (3, 6), (4, 10), (5, 15), (6, 21)];

        let mut result = 0.0;
        if let Some(coefficients) = fit_polynomial(&samples, 3) {
            result = evaluate_polynomial(&coefficients, 7).round();
        } else {
            println!("Failed to fit polynomial");
        }
        assert_eq!(28.0, result);
    }

    #[test]
    fn polynomial_fitting1() {
        // Example: Fit a quadratic polynomial to the provided samples
        let samples = vec![(1, 0), (2, 3), (3, 6), (4, 9), (5, 12), (6, 15)];

        let mut result = 0.0;
        if let Some(coefficients) = fit_polynomial(&samples, 4) {
            result = evaluate_polynomial(&coefficients, 7).round();
        } else {
            println!("Failed to fit polynomial");
        }
        assert_eq!(18.0, result);
    }

    #[test]
    fn extrapolate_line_new_value() {
        let input = vec![10, 13, 16, 21, 30, 45];
        let result = extrapolate_next_value(input);
        assert_eq!(68, result);
    }
    #[test]
    fn extrapolate_line_new_previous_value() {
        let input = vec![10, 13, 16, 21, 30, 45];
        let result = extrapolate_previous_value(input);
        assert_eq!(5, result);
    }

    #[test]
    fn line_to_sequence_test() {
        let input = "10 13 16 21 30 45";
        let result = line_to_sequence(input);
        assert_eq!(vec![10, 13, 16, 21, 30, 45], result);
    }

    #[test]
    fn adjacent_numbers_diffs_test() {
        let input = vec![10, 13, 16, 21, 30, 45];
        let result = adjacent_numbers_diffs(&input);
        assert_eq!(vec![3, 3, 5, 9, 15], result);
    }
}
