use aoc_2023::read_file;

fn main() {
    let input = read_file("inputs/in_day5.txt");
    let sections: Vec<&str> = input.split("\n\n").collect();
    let mut sections_it = sections.iter();

    let seeds = find_seeds(&sections_it.next().unwrap());

    let seed_to_soil_map = sections_it.next()
        .map(|&section| get_mapping_numbers_from_section(section))
        .map(|mapping_numbers| parse_mapping(mapping_numbers))
        .unwrap();

    let soil_to_fertilizer_map = sections_it.next()
        .map(|&section| get_mapping_numbers_from_section(section))
        .map(|mapping_numbers| parse_mapping(mapping_numbers))
        .unwrap();

    let fertilizer_to_water_map = sections_it.next()
        .map(|&section| get_mapping_numbers_from_section(section))
        .map(|mapping_numbers| parse_mapping(mapping_numbers))
        .unwrap();

    let water_to_light_map = sections_it.next()
        .map(|&section| get_mapping_numbers_from_section(section))
        .map(|mapping_numbers| parse_mapping(mapping_numbers))
        .unwrap();

    let light_to_temperature_map = sections_it.next()
        .map(|&section| get_mapping_numbers_from_section(section))
        .map(|mapping_numbers| parse_mapping(mapping_numbers))
        .unwrap();

    let temperature_to_humidity_map = sections_it.next()
        .map(|&section| get_mapping_numbers_from_section(section))
        .map(|mapping_numbers| parse_mapping(mapping_numbers))
        .unwrap();

    let humidity_to_location_map = sections_it.next()
        .map(|&section| get_mapping_numbers_from_section(section))
        .map(|mapping_numbers| parse_mapping(mapping_numbers))
        .unwrap();

    let min_location_part1: u64 = seeds.iter()
        .map(|seed| map_source_to_destination(*seed, &seed_to_soil_map))
        .map(|soil| map_source_to_destination(soil, &soil_to_fertilizer_map))
        .map(|fertilizer| map_source_to_destination(fertilizer, &fertilizer_to_water_map))
        .map(|water| map_source_to_destination(water, &water_to_light_map))
        .map(|light| map_source_to_destination(light, &light_to_temperature_map))
        .map(|temperature| map_source_to_destination(temperature, &temperature_to_humidity_map))
        .map(|humidity| map_source_to_destination(humidity, &humidity_to_location_map))
        .min()
        .unwrap();

    println!(
        "PART1:\n  Minimal location number is {}",
        min_location_part1
    );

    let seed_ranges = find_seed_ranges(&seeds);

    let min_location_part2: u64 = seed_ranges.into_iter()
        .flat_map(|seed_range_def| {
            (seed_range_def.start..seed_range_def.start + seed_range_def.len)
                .map(|seed| {
                    let soil = map_source_to_destination(seed as u64, &seed_to_soil_map);
                    let fertilizer = map_source_to_destination(soil, &soil_to_fertilizer_map);
                    let water = map_source_to_destination(fertilizer, &fertilizer_to_water_map);
                    let light = map_source_to_destination(water, &water_to_light_map);
                    let temperature = map_source_to_destination(light, &light_to_temperature_map);
                    let humidity = map_source_to_destination(temperature, &temperature_to_humidity_map);
                    map_source_to_destination(humidity, &humidity_to_location_map)
                })
                .min()
        })
        .min()
        .unwrap();

    println!(
        "PART2:\n  Minimal location number is {}",
        min_location_part2
    );
}

struct ConvertDefinition {
    source_start: u64,
    destination_start: u64,
    len: u64,
}

fn get_mapping_numbers_from_section(section: &str) -> Vec<&str> {
    section.lines().skip(1).collect()
}

fn map_source_to_destination(source: u64, conv_defs: &Vec<ConvertDefinition>) -> u64 {
    let mut destination: u64 = source;
    if let Some(conv_def) = conv_defs.iter()
        .find(|_conv_def| (_conv_def.source_start.._conv_def.source_start + _conv_def.len).contains(&source)) {
        if let Some(offset) = source.checked_sub(conv_def.source_start) {
            if let Some(new_destination) = conv_def.destination_start.checked_add(offset) {
                destination = new_destination;
            } else {
                // Handle overflow case (optional)
                println!("Overflow occurred while computing destination.");
            }
        } else {
            // Handle underflow case (optional)
            println!("Underflow occurred while computing offset.");
        }
    }
    return destination;
}

struct SeedsRange {
    start: usize,
    len: usize,
}

fn find_seed_ranges(seed_numbers: &Vec<u64>) -> Vec<SeedsRange> {
    seed_numbers.chunks(2).map(|pair| SeedsRange { start: pair[0] as usize, len: pair[1] as usize }).collect()
}

fn find_seeds(input: &str) -> Vec<u64> {
    let seeds_str: Vec<&str> = input
        .split_once(":").unwrap().1
        .trim()
        .split_whitespace()
        .collect();
    let seeds_destinations: Vec<u64> = seeds_str.into_iter().map(|num_str| num_str.parse::<u64>().unwrap()).collect();
    return seeds_destinations;
}

fn parse_mapping(map_lines: Vec<&str>) -> Vec<ConvertDefinition> {
    let mut mappings: Vec<ConvertDefinition> = vec![];
    for map_line in map_lines {
        let split_nums_str: Vec<&str> = map_line.split_whitespace().collect();
        let split_nums: Vec<u64> = split_nums_str.into_iter().map(|num_str| num_str.parse::<u64>().unwrap()).collect();

        mappings.push(ConvertDefinition { source_start: split_nums[1], destination_start: split_nums[0], len: split_nums[2] });
    }
    mappings
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn seeds_parsing() {
        let input = "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

";
        assert_eq!(vec![79, 14, 55, 13], find_seeds(input));
    }

    #[test]
    fn mapping_source_to_destination() {
        let map_str = "50 98 2
52 50 48";
        let mapping_lines: Vec<&str> = map_str.lines().collect();
        let map = parse_mapping(mapping_lines);
        let mut destination = map_source_to_destination(79, &map);
        assert_eq!(81, destination);
        destination = map_source_to_destination(3, &map);
        assert_eq!(3, destination);
    }
}
