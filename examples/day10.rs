use std::cmp::min;
use std::collections::HashSet;
use aoc_2023::read_file;

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
struct Cord {
    x: usize,
    y: usize,
}

struct PipeLoop {
    current_pos: Cord,
    previous_pos: Cord,
    translations_cnt: usize,
}

struct PipePlan {
    plan: Vec<Vec<char>>,
    start: Cord,
}

impl PipePlan {
    fn from_lines(lines: Vec<&str>) -> PipePlan {
        let mut start: Option<Cord> = None;
        let plan: Vec<Vec<char>> = lines
            .into_iter()
            .enumerate()
            .map(|(row, line)| {
                if let Some(column) = line.find('S') {
                    start = Some(Cord { x: column, y: row });
                }
                line.chars().collect()
            })
            .collect();
        if start.is_none() { panic!("Not found start in input") }

        return PipePlan { plan, start: start.expect("Not found start in input") };
    }
    fn get_pipe(&self, cord: Cord) -> char { return self.plan[cord.y][cord.x]; }
}

fn collect_loops_begins(plan: &PipePlan) -> Vec<Cord> {
    let mut loop_begins: Vec<Cord> = vec![];

    let mut potential_next_cord = Cord { x: plan.start.x, y: plan.start.y + 1 };
    if vec!['F', '7', '|'].contains(&plan.get_pipe(potential_next_cord)) {
        loop_begins.push(potential_next_cord);
    }

    if plan.start.y > 0 {
        potential_next_cord.y = plan.start.y - 1;
        if vec!['L', 'J', '|'].contains(&plan.get_pipe(potential_next_cord)) {
            loop_begins.push(potential_next_cord);
        }
    }

    potential_next_cord.x = plan.start.x + 1;
    potential_next_cord.y = plan.start.y;
    if vec!['J', '7', '-'].contains(&plan.get_pipe(potential_next_cord)) {
        loop_begins.push(potential_next_cord);
    }

    if plan.start.x > 0 {
        potential_next_cord.x = plan.start.x - 1;
        if vec!['-', 'L', 'F'].contains(&plan.get_pipe(potential_next_cord)) {
            loop_begins.push(potential_next_cord);
        }
    }

    return loop_begins;
}

impl PipeLoop {
    fn translate(&mut self, pipe_type: char) {
        let mut next_pos: Cord = Cord { x: 0, y: 0 };
        match pipe_type {
            '|' => {
                if self.previous_pos.x != self.current_pos.x { panic!("Wrong pipe connection {}", pipe_type) }
                next_pos.x = self.current_pos.x;
                if self.previous_pos.y > self.current_pos.y { // from north to south
                    next_pos.y = self.current_pos.y - 1;
                } else {                                      // from south to north
                    next_pos.y = self.current_pos.y + 1;
                }
            }
            '-' => {
                if self.previous_pos.y != self.current_pos.y {
                    panic!("Wrong pipe connection {}", pipe_type)
                }
                next_pos.y = self.current_pos.y;
                if self.previous_pos.x > self.current_pos.x { // from west to east
                    next_pos.x = self.current_pos.x - 1;
                } else {                                      // from east to west
                    next_pos.x = self.current_pos.x + 1;
                }
            }
            'L' => {
                if self.previous_pos.x == self.current_pos.x {       // from north to south-east
                    next_pos.x = self.current_pos.x + 1;
                    next_pos.y = self.current_pos.y;
                } else if self.previous_pos.x > self.current_pos.x { // from east north-west
                    next_pos.x = self.current_pos.x;
                    next_pos.y = self.current_pos.y - 1;
                } else {
                    panic!("Wrong pipe connection {}", pipe_type)
                }
            }
            'J' => {
                if self.previous_pos.x == self.current_pos.x {       // from north to south-west
                    next_pos.x = self.current_pos.x - 1;
                    next_pos.y = self.current_pos.y;
                } else if self.previous_pos.x < self.current_pos.x { // from east north-east
                    next_pos.x = self.current_pos.x;
                    next_pos.y = self.current_pos.y - 1;
                } else {
                    panic!("Wrong pipe connection {}", pipe_type)
                }
            }
            '7' => {
                if self.previous_pos.x == self.current_pos.x {       // from south to north-west
                    next_pos.x = self.current_pos.x - 1;
                    next_pos.y = self.current_pos.y;
                } else if self.previous_pos.x < self.current_pos.x { // from west to south-east
                    next_pos.x = self.current_pos.x;
                    next_pos.y = self.current_pos.y + 1;
                } else {
                    panic!("Wrong pipe connection {}", pipe_type)
                }
            }
            'F' => {
                if self.previous_pos.x == self.current_pos.x {       // from south to north-east
                    next_pos.x = self.current_pos.x + 1;
                    next_pos.y = self.current_pos.y;
                } else if self.previous_pos.x > self.current_pos.x { // from east to south-west
                    next_pos.x = self.current_pos.x;
                    next_pos.y = self.current_pos.y + 1;
                } else {
                    panic!("Wrong pipe connection {}", pipe_type)
                }
            }
            _ => { panic!("Not supported pipe type (sign) {}", pipe_type) }
        }
        self.translations_cnt += 1;
        self.previous_pos = self.current_pos.clone();
        self.current_pos = next_pos.clone();
    }
}

fn get_nest_candidates(path: &Vec<(char, Cord)>) -> HashSet<(Cord)> {
    let mut result = HashSet::new();
    for position in path {
        result.extend(get_neighbours(&position.1));
    }
    result
}

fn get_neighbours(coordinates: &Cord) -> HashSet<(Cord)> {
    let mut vec: Vec<Cord> = vec![];
    if !coordinates.x.overflowing_sub(1).1 {
        vec.push(Cord { x: coordinates.x.overflowing_sub(1).0, y: coordinates.y })
    }
    if !coordinates.x.overflowing_add(1).1 {
        vec.push(Cord { x: coordinates.x.overflowing_add(1).0, y: coordinates.y })
    }
    if !coordinates.y.overflowing_sub(1).1 {
        vec.push(Cord { x: coordinates.x, y: coordinates.y.overflowing_sub(1).0 })
    }
    if !coordinates.y.overflowing_add(1).1 {
        vec.push(Cord { x: coordinates.x, y: coordinates.y.overflowing_add(1).0 })
    }

    return HashSet::from_iter(vec.into_iter())
}

fn check_if_nest_horizontally(coordinates: &Cord, path: &Vec<(char, Cord)>) -> bool {
    let mut north_connections_to_west = 0;
    let mut south_connections_to_west = 0;
    for path_element in path {
        if path_element.1.x == coordinates.x && path_element.1.y < coordinates.y {
            if vec!['|', 'L', 'J'].contains(&path_element.0) {
                north_connections_to_west += 1;
            }
            if vec!['|', '7', 'F'].contains(&path_element.0) {
                south_connections_to_west += 1;
            }
        }
    }
    let north_south_connections_to_west = min(north_connections_to_west, south_connections_to_west);
    north_south_connections_to_west % 2 == 1
}

fn main() {
    let input = read_file("inputs/in_day10.txt");

    let lines: Vec<&str> = input.lines().collect();
    let plan = PipePlan::from_lines(lines);
    {
        let mut pipe_loop: PipeLoop = PipeLoop {
            current_pos: Cord { x: 0, y: 0 },
            previous_pos: Cord { x: 0, y: 0 },
            translations_cnt: 1,
        };
        let mut longest_loop: usize = 0;
        for begin in collect_loops_begins(&plan) {
            pipe_loop.previous_pos = plan.start;
            pipe_loop.current_pos = begin;

            loop {
                let pipe = plan.get_pipe(pipe_loop.current_pos);
                if pipe == 'S' { break; }
                pipe_loop.translate(pipe);
            }
            println!("For begin {:?} translation counts {}", begin, pipe_loop.translations_cnt);
            longest_loop = longest_loop.max(pipe_loop.translations_cnt);
            pipe_loop.translations_cnt = 1;
        }

        println!("PART1:\n Longest loop: {}", longest_loop / 2);
    }

    {
        let mut pipe_loop: PipeLoop = PipeLoop {
            current_pos: Cord { x: 0, y: 0 },
            previous_pos: Cord { x: 0, y: 0 },
            translations_cnt: 1,
        };
        let possible_loops_begins = collect_loops_begins(&plan);
        let mut longest_loop: usize = 0;
        let mut paths: Vec<(char, Cord)> = vec![];
        for (idx, begin) in possible_loops_begins.into_iter().enumerate() {
            pipe_loop.previous_pos = plan.start;
            pipe_loop.current_pos = begin;

            loop {
                let pipe = plan.get_pipe(pipe_loop.current_pos);
                if pipe == 'S' { break; }
                paths.push((pipe, pipe_loop.current_pos));
                pipe_loop.translate(pipe);
            }
            paths.push(('S', plan.start));
            println!("For begin {:?} translation counts {}", begin, pipe_loop.translations_cnt);
            longest_loop = longest_loop.max(pipe_loop.translations_cnt);
            pipe_loop.translations_cnt = 1;
        }

        let mut nest_candidates = get_nest_candidates(&paths);
        let mut checked_fields = HashSet::from_iter(paths.iter().map(|path_element| path_element.1));
        let mut possible_nest_locations = 1;
        nest_candidates = nest_candidates.difference(&checked_fields).map(|element| *element).collect();
        while !nest_candidates.is_empty() {
            let mut new_nest_candiadtes = HashSet::new();
            for candidate in &nest_candidates {
                if check_if_nest_horizontally(&candidate, &paths) {
                    possible_nest_locations += 1;
                    new_nest_candiadtes.extend(get_neighbours(&candidate));
                    checked_fields.insert(*candidate);
                }
            }
            nest_candidates = new_nest_candiadtes.difference(&checked_fields).map(|element| *element).collect();
        }

        println!("PART2:\n Possible nest locations: {}", possible_nest_locations);
    }
}
