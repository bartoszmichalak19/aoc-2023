use aoc_2023::read_file;

fn main() {
    let input = read_file("inputs/in_day4.txt");
    let scratchcards_lines: Vec<&str> = input.lines().collect();

    {
        let mut points_sum: u32 = 0;
        for line in &scratchcards_lines {
            let matches_qty = find_matches_qty(line);
            if matches_qty > 0 {
                points_sum += 2_i32.pow(matches_qty - 1) as u32;
            }
        }

        println!(
            "PART1:\n   Sum of scratch points is {}",
            points_sum
        );
    }

    {
        // /*        PART2       */
        let mut cards_holder: Vec<u32> = vec![1; scratchcards_lines.len()];
        for (idx, line) in scratchcards_lines.iter().enumerate() {
            let matches_qty = find_matches_qty(line) as usize;
            for i in idx + 1..idx + matches_qty + 1 {
                cards_holder[i] += cards_holder[idx];
            }
        }
        let cards_sum: u32 = cards_holder.iter().sum();
        println!(
            "PART2:\n   Total sum of scratch cards is {}",
            cards_sum
        );
    }
}

fn find_matches_qty(scratchcard_line: &str) -> u32 {
    let Some((win_nums_str, my_nums_str)) = scratchcard_line.split_once(":").unwrap().1
        .split_once("|")
        else { panic!("Separator not found in the line"); };
    let win_nums_splitted: Vec<&str> = win_nums_str.split_whitespace().collect();
    let my_nums_str_splitted: Vec<&str> = my_nums_str.split_whitespace().collect();

    let matches_qty = my_nums_str_splitted.into_iter()
        .filter(|scratched_number_str| win_nums_splitted.contains(scratched_number_str))
        .collect::<Vec<_>>()
        .len();
    return matches_qty as u32;
}

