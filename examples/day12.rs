use std::collections::HashMap;

fn count_arrangements(row: &str, groups: &[usize]) -> usize {
    let mut dp: HashMap<(usize, usize), usize> = HashMap::new();

    dp.insert((0, 0), 1);

    for (i, &group) in groups.iter().enumerate() {
        for j in 0..=row.len() {
            for k in 0..=group {
                if let Some(&arrangements) = dp.get(&(j, k)) {
                    dp.entry((j + 1, k + 1))
                        .and_modify(|e| *e += arrangements)
                        .or_insert(arrangements);
                }

                if k < group {
                    if let Some(&arrangements) = dp.get(&(j, k)) {
                        dp.entry((j + 1, 0))
                            .and_modify(|e| *e += arrangements)
                            .or_insert(arrangements);
                    }
                }
            }
        }
    }

    *dp.get(&(row.len(), 0)).unwrap_or(&0)
}

fn main() {
    let rows = vec![
        "???.### 1,1,3",
        ".??..??...?##. 1,1,3",
        "?#?#?#?#?#?#?#? 1,3,1,6",
        "????.#...#... 4,1,1",
        "????.######..#####. 1,6,5",
        "?###???????? 3,2,1",
    ];

    let total_arrangements: usize = rows
        .iter()
        .map(|row| {
            let parts: Vec<&str> = row.split_whitespace().collect();
            let spring_part = parts[0];
            let groups_part = parts[1];

            let groups: Vec<usize> = groups_part
                .split(',')
                .filter_map(|s| s.parse().ok())
                .collect();

            count_arrangements(spring_part, &groups)
        })
        .sum();

    println!("Total arrangements: {}", total_arrangements);
}
