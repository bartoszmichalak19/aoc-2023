use std::cmp::{max, min};
use std::collections::HashMap;
use aoc_2023::read_file;

type Node = String;

// Function to navigate the network based on left/right instructions
fn navigate(network: &HashMap<Node, (Node, Node)>, current: Node, instruction: char) -> Node {
    match instruction {
        'L' => network.get(&current).map_or(current, |(left, _)| left.to_string()),
        'R' => network.get(&current).map_or(current, |(_, right)| right.to_string()),
        _ => current,
    }
}

fn count_escape_moves(network: &HashMap<Node, (Node, Node)>, start_node: Node, target: Node, instructions: &str) -> u32 {
    let mut steps: u32 = 0;
    let mut current_node = start_node;
    while current_node != target
    {
        for instruction in instructions.chars() {
            current_node = navigate(&network, current_node, instruction);
            steps += 1;
            if current_node == target {
                break;
            }
        }
    }
    steps
}

fn gcd(first: u64, second: u64) -> u64 {
    let mut min = min(first, second);
    let mut max = max(first, second);

    loop {
        let r = max % min;

        if r == 0 {
            return min;
        }

        max = min;
        min = r;
    }
}

fn lcm(first: u64, second: u64) -> u64 {
    first * second / gcd(first, second)
}


fn count_escape_moves_part2(network: &HashMap<Node, (Node, Node)>, start_nodes: Vec<Node>, instructions: &str) -> u64 {
    let mut steps: u64 = 1;
    let mut tmp_steps: u64;
    for start_node in start_nodes {
        let mut current_node = start_node;
        tmp_steps = 0;
        while current_node.chars().last().unwrap() != 'Z'
        {
            for instruction in instructions.chars() {
                current_node = navigate(&network, current_node, instruction);
                tmp_steps += 1;
                if current_node.chars().last().unwrap() == 'Z' {
                    break;
                }
            }
        }
        steps = lcm(tmp_steps, steps);
    }

    steps
}

fn create_network(input: &str, network: &mut HashMap<Node, (Node, Node)>, part2_start_nodes: &mut Vec<Node>) {
    let node_lines = input.lines();
    node_lines.for_each(|line|
        {
            let parts: Vec<&str> = line.split(" = ").collect();
            let current = parts[0].trim().to_string();
            let pair = parts[1].trim_matches(|c| c == '(' || c == ')').split(", ").collect::<Vec<&str>>();
            let left = pair[0].to_string();
            let right = pair[1].to_string();
            network.insert(current.clone(), (left.clone(), right.clone()));
            if current.chars().last().unwrap() == 'A' {
                part2_start_nodes.push(current.clone());
            }
        }
    )
}

fn main() {
    let mut network = HashMap::new();
    let input = read_file("inputs/in_day8.txt");
    let sections: Vec<&str> = input.split("\n\n").collect();
    let mut sections_it = sections.iter();
    let instructions: &str = sections_it.next().unwrap();
    let mut start_nodes_part2: Vec<Node> = vec![];
    create_network(&sections_it.next().unwrap(), &mut network, &mut start_nodes_part2);

    let target_node = "ZZZ";
    // Navigate the network and count steps
    let start_node = "AAA";

    let steps = count_escape_moves(&network, start_node.to_string(), target_node.to_string(), instructions);

    println!("PART1:\n Steps required to reach ZZZ: {}", steps);

    let steps_part2 = count_escape_moves_part2(&network, start_nodes_part2, instructions);

    println!("PART2:\n Steps required to reach XXZ: {}", steps_part2);
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example1_part1() {
        // Define the network of labeled nodes
        let mut network = HashMap::new();
        network.insert("AAA".to_string(), ("BBB".to_string(), "CCC".to_string()));
        network.insert("BBB".to_string(), ("DDD".to_string(), "EEE".to_string()));
        network.insert("CCC".to_string(), ("ZZZ".to_string(), "GGG".to_string()));
        network.insert("DDD".to_string(), ("DDD".to_string(), "DDD".to_string()));
        network.insert("EEE".to_string(), ("EEE".to_string(), "EEE".to_string()));
        network.insert("GGG".to_string(), ("GGG".to_string(), "GGG".to_string()));
        network.insert("ZZZ".to_string(), ("ZZZ".to_string(), "ZZZ".to_string()));

        assert_eq!(2, count_escape_moves(&network, "AAA".to_string(), "ZZZ".to_string(), "RL"));
    }

    #[test]
    fn example1_part2() {
        let input = "11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";

        let mut network = HashMap::new();
        let instructions: &str = "LR";
        let mut start_nodes_part2: Vec<Node> = vec![];
        create_network(input, &mut network, &mut start_nodes_part2);

        let steps_part2 = count_escape_moves_part2(&network, start_nodes_part2, instructions);

        println!("PART2:\n Steps required to reach XXZ: {}", steps_part2);
    }
}