use aoc_2023::read_file;

fn find_reflection_line_index(matrix: &Vec<Vec<char>>) -> usize {
    for row_index in 1..matrix.len() {
        let a: Vec<_> = matrix[..row_index].iter().rev().collect();
        let b: Vec<_> = matrix[row_index..].iter().collect();

        let above = a.iter().take(b.len()).cloned().collect::<Vec<_>>();
        let below = b.iter().take(above.len()).cloned().collect::<Vec<_>>();

        if above == below {
            return row_index;
        }
    }
    return 0;
}

fn find_reflection_line_index_part2(matrix: &Vec<Vec<char>>) -> usize {
    for row_index in 1..matrix.len() {
        let a: Vec<_> = matrix[..row_index].iter().rev().collect();
        let b: Vec<_> = matrix[row_index..].iter().collect();

        let above = a.iter().take(b.len()).cloned().collect::<Vec<_>>();
        let below = b.iter().take(above.len()).cloned().collect::<Vec<_>>();

        let diff_count = above
            .iter()
            .zip(below.iter())
            .map(|(a, b)| a.iter().zip(b.iter()).filter(|(x, y)| x != y).count())
            .sum::<usize>();

        if diff_count == 1 {
            return row_index;
        }
    }
    return 0;
}

struct PatternMap {
    pattrens_map: Vec<Vec<char>>,
}

impl PatternMap {
    fn read_from_lines(&mut self, lines: Vec<&str>) {
        self.pattrens_map = lines
            .into_iter()
            .map(|line| line.chars().collect())
            .collect();
    }
}

fn main() {
    let input = read_file("inputs/in_day13.txt");

    let mut total_part1: usize = 0;
    let mut total_part2: usize = 0;
    let sections: Vec<&str> = input.split("\n\n").collect();
    sections.into_iter().for_each(|section| {
        let lines: Vec<&str> = section.lines().collect();
        let mut map: PatternMap = PatternMap { pattrens_map: vec![vec![]] };
        map.read_from_lines(lines);
        let row = find_reflection_line_index(&map.pattrens_map);
        total_part1 += row * 100;
        let row = find_reflection_line_index_part2(&map.pattrens_map);
        total_part2 += row * 100;


        let mut reversed_matrix: Vec<Vec<char>> = vec![];
        for col_index in 0..map.pattrens_map.first().unwrap().len() {
            reversed_matrix.push(map.pattrens_map.iter().map(|row| row[col_index]).collect());
        }
        let col = find_reflection_line_index(&reversed_matrix);
        total_part1 += col;
        let col = find_reflection_line_index_part2(&reversed_matrix);
        total_part2 += col;
    });

    println!("PART1: \nTotal sum of notes: {}", total_part1);
    println!("\nPART2: \nTotal sum of notes: {}", total_part2);
}
