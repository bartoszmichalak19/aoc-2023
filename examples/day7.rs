use aoc_2023::read_file;

mod part1 {
    use std::collections::HashMap;

    #[derive(Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Clone)]
    pub(crate) enum Card {
        Number(u8),
        T,
        J,
        Q,
        K,
        A,
    }

    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
    pub(crate) enum Rank {
        Five,
        Four,
        Full,
        Three,
        Pairs,
        Pair,
        NoFigure,
    }


    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
    pub(crate) struct Hand {
        pub(crate) cards: Vec<Card>,
        pub(crate) bid: usize,
    }

    impl Hand {
        pub(crate) fn from_str(s: &str, bid: usize) -> Hand {
            let cards = s.chars().map(|c| match c {
                'A' => Card::A,
                'K' => Card::K,
                'Q' => Card::Q,
                'J' => Card::J,
                'T' => Card::T,
                '2'..='9' => Card::Number(c.to_digit(10).unwrap() as u8),
                _ => panic!("Invalid card label"),
            }).collect();
            Hand { cards, bid }
        }
    }

    pub(crate) fn rank_hand(hand: &Hand) -> (Rank, HashMap<Card, usize>) {
        let mut card_counts: HashMap<Card, usize> = HashMap::new();
        for card in &hand.cards {
            if let Some(x) = card_counts.get_mut(&card) {
                *x += 1;
            } else { card_counts.insert(card.clone(), 1); }
        }

        for (_, count) in card_counts.iter() {
            if *count == 5 {
                return (Rank::Five, card_counts);
            }
        }

        for (_, count) in card_counts.iter() {
            if *count == 4 {
                return (Rank::Four, card_counts);
            }
        }

        for (_, count) in card_counts.iter() {
            if *count == 3 {
                for (_, other_count) in card_counts.iter() {
                    if *other_count == 2 {
                        return (Rank::Full, card_counts);
                    }
                }
                return (Rank::Three, card_counts);
            }
        }

        for (card1, count) in card_counts.iter() {
            if *count == 2 {
                for (card2, other_count) in card_counts.iter() {
                    if *other_count == 2 && card1 != card2 {
                        return (Rank::Pairs, card_counts);
                    }
                }
                return (Rank::Pair, card_counts);
            }
        }
        (Rank::NoFigure, card_counts)
    }
}

mod part2 {
    use std::collections::HashMap;

    #[derive(Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Clone)]
    pub(crate) enum Card {
        J,
        Number(u8),
        T,
        Q,
        K,
        A,
    }

    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
    pub(crate) enum Rank {
        Five,
        Four,
        Full,
        Three,
        Pairs,
        Pair,
        NoFigure,
    }


    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
    pub(crate) struct Hand {
        pub(crate) cards: Vec<Card>,
        pub(crate) bid: usize,
    }

    impl Hand {
        pub(crate) fn from_str(s: &str, bid: usize) -> Hand {
            let cards = s.chars().map(|c| match c {
                'A' => Card::A,
                'K' => Card::K,
                'Q' => Card::Q,
                'J' => Card::J,
                'T' => Card::T,
                '2'..='9' => Card::Number(c.to_digit(10).unwrap() as u8),
                _ => panic!("Invalid card label"),
            }).collect();
            Hand { cards, bid }
        }
    }

    fn rank_hand(hand: &Hand) -> (Rank, HashMap<Card, usize>) {
        let mut card_counts: HashMap<Card, usize> = HashMap::new();
        for card in &hand.cards {
            if let Some(x) = card_counts.get_mut(&card) {
                *x += 1;
            } else { card_counts.insert(card.clone(), 1); }
        }

        for (_, count) in card_counts.iter() {
            if *count == 5 {
                return (Rank::Five, card_counts);
            }
        }

        for (card, count) in card_counts.iter() {
            if *count == 4 && card != &Card::J {
                return (Rank::Four, card_counts);
            }
        }

        for (card, count) in card_counts.iter() {
            if *count == 3 && card != &Card::J {
                for (other_card, other_count) in card_counts.iter() {
                    if *other_count == 2 && other_card != &Card::J {
                        return (Rank::Full, card_counts);
                    }
                }
                return (Rank::Three, card_counts);
            }
        }

        for (card1, count) in card_counts.iter() {
            if *count == 2 && card1 != &Card::J {
                for (card2, other_count) in card_counts.iter() {
                    if *other_count == 2 && card2 != &Card::J && card1 != card2 {
                        return (Rank::Pairs, card_counts);
                    }
                }
                return (Rank::Pair, card_counts);
            }
        }
        (Rank::NoFigure, card_counts)
    }

    pub(crate) fn rank_hand_part2(hand: &Hand) -> (Rank, HashMap<Card, usize>) {
        let (rank, counts) = rank_hand(hand);
        let mut rank_with_joker = rank;
        if let Some(joker_count) = counts.get(&Card::J) {
            if *joker_count > 0 {
                rank_with_joker = match rank {
                    Rank::Five => Rank::Five,
                    Rank::Four => {
                        if *joker_count == 1 {
                            Rank::Five
                        } else { panic!("Invalid joker and rank"); }
                    }
                    Rank::Full => Rank::Full,
                    Rank::Three => {
                        if *joker_count == 1 {
                            Rank::Four
                        } else if *joker_count == 2 {
                            Rank::Five
                        } else { panic!("Invalid joker and rank"); }
                    }
                    Rank::Pairs => {
                        if *joker_count == 1 {
                            Rank::Full
                        } else {
                            panic!("Invalid joker and rank");
                        }
                    }
                    Rank::Pair => {
                        if *joker_count == 1 {
                            Rank::Three
                        } else if *joker_count == 2 {
                            Rank::Four
                        } else if *joker_count == 3 {
                            Rank::Five
                        } else { panic!("Invalid joker and rank"); }
                    }
                    Rank::NoFigure => {
                        if *joker_count == 1 {
                            Rank::Pair
                        } else if *joker_count == 2 {
                            Rank::Three
                        } else if *joker_count == 3 {
                            Rank::Four
                        } else if *joker_count == 4 {
                            Rank::Five
                        } else { panic!("Invalid joker and rank"); }
                    }
                }
            }
        }

        return (rank_with_joker, counts);
    }
}

fn main() {
    let input = read_file("inputs/in_day7.txt");
    let input: Vec<(&str, usize)> = input
        .lines()
        .map(|line| {
            let parts: Vec<&str> = line.split_whitespace().collect();
            let bid = parts[1].parse::<usize>().unwrap();
            let hand = parts[0];
            (hand, bid)
        })
        .collect();
    {
        let mut hands: Vec<part1::Hand> = input.iter().map(|(s, bid)| part1::Hand::from_str(s, *bid)).collect();
        hands.sort_by(|a, b| {
            let (rank_a, card_counts_a) = part1::rank_hand(a);
            let (rank_b, card_counts_b) = part1::rank_hand(b);

            if rank_a != rank_b {
                rank_a.cmp(&rank_b)
            } else {
                for (card_a, card_b) in a.cards.iter().zip(b.cards.iter()) {
                    match card_b.cmp(&card_a) {
                        std::cmp::Ordering::Equal => continue,
                        other => return other,
                    }
                }

                std::cmp::Ordering::Equal
            }
        });

        let total_winnings: usize = hands
            .iter()
            .enumerate()
            .map(|(i, hand)| {
                hand.bid * (hands.len() - i)
            })
            .sum();

        println!("PART1: \nTotal Winnings: {}", total_winnings);
    }

    {
        let mut hands: Vec<part2::Hand> = input.iter().map(|(s, bid)| part2::Hand::from_str(s, *bid)).collect();
        hands.sort_by(|a, b| {
            let (rank_a, card_counts_a) = part2::rank_hand_part2(a);
            let (rank_b, card_counts_b) = part2::rank_hand_part2(b);

            if rank_a != rank_b {
                rank_a.cmp(&rank_b)
            } else {
                for (card_a, card_b) in a.cards.iter().zip(b.cards.iter()) {
                    match card_b.cmp(&card_a) {
                        std::cmp::Ordering::Equal => continue,
                        other => return other,
                    }
                }

                std::cmp::Ordering::Equal
            }
        });

        let total_winnings: usize = hands
            .iter()
            .enumerate()
            .map(|(i, hand)| {
                hand.bid * (hands.len() - i)
            })
            .sum();

        println!("PART2: \nTotal Winnings: {}", total_winnings);
    }
}

#[cfg(test)]
mod testss {
    use super::*;

    #[test]
    fn compare_two_cards() {
        assert_eq!(part1::Card::A.cmp(&part1::Card::A), std::cmp::Ordering::Equal);
        assert!(part1::Card::Number(9) > part1::Card::Number(2));
        assert!(part1::Card::J > part1::Card::Number(2));
    }

    #[test]
    fn test_card_ordering() {
        assert!(part1::Card::A > part1::Card::K);
        assert!(part1::Card::K > part1::Card::Q);
        assert!(part1::Card::Q > part1::Card::J);
        assert!(part1::Card::J > part1::Card::T);
        assert!(part1::Card::T > part1::Card::Number(5));

        assert_eq!(part1::Card::A, part1::Card::A);
        assert_eq!(part1::Card::Number(3), part1::Card::Number(3));
        assert_eq!(part1::Card::K, part1::Card::K);
        assert_ne!(part1::Card::J, part1::Card::Q);

        assert!(part1::Card::Number(2) < part1::Card::A);
        assert!(part1::Card::Number(5) < part1::Card::K);

        let mut cards = vec![
            part1::Card::Number(5),
            part1::Card::T,
            part1::Card::J,
            part1::Card::K,
            part1::Card::Q,
            part1::Card::A,
        ];

        cards.sort();
        let rev_cards = cards.reverse();
    }

    #[test]
    fn compare_same_rank_hand_test() {
        let input = vec![
            ("A3AAA", 4),
            ("QAAAA", 1),
            ("AAAA9", 3),
            ("AAAAJ", 2),
            ("A2AAA", 5),
        ];


        let mut hands: Vec<part1::Hand> = input.iter().map(|(s, bid)| part1::Hand::from_str(s, *bid)).collect();
        let mut sorted_card_counts_a: Vec<(part1::Card, usize)> = vec![];
        let mut sorted_card_counts_b: Vec<(part1::Card, usize)> = vec![];
        let mut sorted_rest_cards_a: Vec<part1::Card> = vec![];
        hands.sort_by(|a, b| {
            let (rank_a, card_counts_a) = part1::rank_hand(a);
            let (rank_b, card_counts_b) = part1::rank_hand(b);

            if rank_a != rank_b {
                rank_a.cmp(&rank_b)
            } else {
                sorted_card_counts_a = card_counts_a.into_iter().collect();
                sorted_card_counts_b = card_counts_b.into_iter().collect();

                sorted_card_counts_a.sort_by(|a, b| a.1.cmp(&b.1).then_with(|| a.0.cmp(&b.0)));
                sorted_card_counts_b.sort_by(|a, b| a.1.cmp(&b.1).then_with(|| a.0.cmp(&b.0)));

                assert_eq!(sorted_card_counts_a.last().unwrap().0, part1::Card::A);
                assert_ne!(sorted_card_counts_a.first().unwrap().0, part1::Card::A);
                sorted_rest_cards_a.push(sorted_card_counts_a.first().unwrap().0.clone());

                for (card_a, card_b) in sorted_card_counts_a.iter().rev().zip(sorted_card_counts_b.iter().rev()) {
                    match card_b.0.cmp(&card_a.0) {
                        std::cmp::Ordering::Equal => continue,
                        other => return other,
                    }
                }
                std::cmp::Ordering::Equal
            }
        });

        let expected_bids_order: Vec<usize> = vec![1, 2, 3, 4, 5];
        let bids_order: Vec<usize> = hands.iter().map(|hand| hand.bid).collect();
        assert_eq!(bids_order, expected_bids_order);
    }
}