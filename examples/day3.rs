use aoc_2023::read_file;

fn main() {
    let input = read_file("inputs/in_day3.txt");

    {
        let engine_schematic_lines: Vec<&str> = input.lines().collect();
        let engine_schematic_chars_matrix: Vec<Vec<char>> = engine_schematic_lines
            .into_iter()
            .map(|line| line.chars().collect())
            .collect(); //vec<vec<char>

        let mut part_numbers_sum: u32 = 0;
        let mut gear_ratio_sum: u32 = 0;
        for i in 0..engine_schematic_chars_matrix.len() {
            for j in 0..engine_schematic_chars_matrix[i].len() {
                let c = engine_schematic_chars_matrix[i][j];
                if is_engine_symbol(c) {
                    let adjacent_part_numbers = get_adjacent_part_numbers(&engine_schematic_chars_matrix, i, j);
                    part_numbers_sum += adjacent_part_numbers.iter().sum::<u32>();
                    if is_gear_ratio_symbol(c) && adjacent_part_numbers.len() == 2 {
                        gear_ratio_sum += adjacent_part_numbers[0] * adjacent_part_numbers[1];
                    }
                }
                if is_gear_ratio_symbol(c) {
                    let adjacent_gear_ratio_numbers = get_adjacent_part_numbers(&engine_schematic_chars_matrix, i, j);
                    if adjacent_gear_ratio_numbers.len() == 2 {}
                }
            }
        }

        println!(
            "PART1:\n   Sum of calibration values is {}",
            part_numbers_sum
        );

        /*        PART2       */

        println!(
            "PART2:\n   Sum of calibration values is {}",
            gear_ratio_sum
        );
    }
}


fn is_engine_symbol(c: char) -> bool {
    return !c.is_ascii_alphanumeric() && c != '.';
}

fn is_gear_ratio_symbol(c: char) -> bool {
    return c == '*';
}

struct AdjacentNumber {
    str: String,
    is_adjacent: bool,
}

fn get_adjacent_part_numbers(schematic: &Vec<Vec<char>>, row: usize, col: usize) -> Vec<u32> {
    let mut adjacent_part_numbers: Vec<u32> = vec![];
    let mut current_number: AdjacentNumber = AdjacentNumber { str: "".to_string(), is_adjacent: false };
    let rows_width = schematic[0].len();
    for i in row - 1..row + 2 {
        for j in 0..rows_width {
            let c = schematic[i][j];
            if c.is_digit(10) {
                current_number.str.push(c);
                if !current_number.is_adjacent &&
                    (col - 1..col + 2).contains(&j) {
                    current_number.is_adjacent = true;
                }
            } else if !current_number.str.is_empty() {
                if let Ok(number) = current_number.str.parse::<u32>() {
                    if current_number.is_adjacent {
                        adjacent_part_numbers.push(number);
                    }
                }
                current_number.is_adjacent = false;
                current_number.str.clear();
            }
        }
    }
    return adjacent_part_numbers;
}