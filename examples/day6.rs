fn main() {
    {
        let input_data_part2: Vec<(u64, u64)> = vec![(51, 222), (92, 2031), (68, 1126), (90, 1225)];
        let margin_product_part2 = calc_marigins_product(input_data_part2);
        println!(
            "PART1:\n   Product of found margin values is {}",
            margin_product_part2
        );
    }

    {
        let input_data_part2: Vec<(u64, u64)> = vec![(51926890, 222203111261225)];
        let margin_product_part2 = calc_marigins_product(input_data_part2);
        println!(
            "PART2:\n   Product of found margin values is {}",
            margin_product_part2
        );
    }
}

fn calc_marigins_product(input_data: Vec<(u64, u64)>) -> u64 {
    input_data.iter().map(|(time, distance)| {
        let mut cnt = 0;
        for v in 1..*time {
            if v * ((*time) - v) > (*distance) {
                cnt += 1;
            }
        }
        return cnt;
    }).product()
}

