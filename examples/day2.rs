use std::fmt;
use aoc_2023::read_file;

enum Color {
    Red,
    Green,
    Blue,
}

impl Color {
    fn from(s: &str) -> Color {
        match s {
            "red" => Color::Red,
            "green" => Color::Green,
            "blue" => Color::Blue,
            _ => panic!("wrong move type"),
        }
    }
}

#[derive(Clone)]
struct Move {
    red_cubes_qty: u32,
    green_cubes_qty: u32,
    blue_cubes_qty: u32,
}

impl fmt::Debug for Move {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Move {{ red: {}, green: {}, blue: {} }}\n", self.red_cubes_qty, self.green_cubes_qty, self.blue_cubes_qty)
    }
}

#[derive(Clone)]
struct Game {
    game_id: u32,
    moves: Vec<Move>,
}

impl fmt::Debug for Game {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Game {{ id: {}, moves: {:?} }}\n", self.game_id, self.moves)
    }
}

fn parse_game(line: &str) -> Game {
    let mut game: Game = Game { game_id: 0, moves: vec![] };
    let Some((game_id_str, moves_str)) = line.split_once(":")
        else { panic!("Separator not found in the line"); };

    game.game_id = game_id_str.split_once(' ').unwrap().1.parse::<u32>().unwrap();

    let splitted_moves: Vec<&str> = moves_str.split(";").collect();
    splitted_moves.into_iter()
        .map(|_move| {
            let mut current_bag_pick: Move = Move {
                red_cubes_qty: 0,
                green_cubes_qty: 0,
                blue_cubes_qty: 0,
            };
            _move.split(',')
                .flat_map(|cubes| cubes.trim().split_once(' '))
                .for_each(|(cubes_qty_str, cube_color_str)| {
                    match Color::from(cube_color_str) {
                        Color::Red => { current_bag_pick.red_cubes_qty = cubes_qty_str.parse::<u32>().unwrap() }
                        Color::Green => { current_bag_pick.green_cubes_qty = cubes_qty_str.parse::<u32>().unwrap() }
                        Color::Blue => { current_bag_pick.blue_cubes_qty = cubes_qty_str.parse::<u32>().unwrap() }
                    }
                });
            current_bag_pick
        })
        .for_each(|current_bag_pick| game.moves.push(current_bag_pick));

    return game;
}

fn validate_game(game: &Game) -> bool {
    return game.moves.iter().all(|_move| {
        _move.red_cubes_qty <= 12 && _move.green_cubes_qty <= 13 && _move.blue_cubes_qty <= 14
    });
}

fn find_least_cubes_for_game(_moves: &Vec<Move>) -> Move {
    let mut fewest_cubes: Move = Move {
        red_cubes_qty: 0,
        green_cubes_qty: 0,
        blue_cubes_qty: 0,
    };

    _moves.iter()
        .for_each(|_move| {
            if fewest_cubes.red_cubes_qty < _move.red_cubes_qty { fewest_cubes.red_cubes_qty = _move.red_cubes_qty; }
            if fewest_cubes.green_cubes_qty < _move.green_cubes_qty { fewest_cubes.green_cubes_qty = _move.green_cubes_qty; }
            if fewest_cubes.blue_cubes_qty < _move.blue_cubes_qty { fewest_cubes.blue_cubes_qty = _move.blue_cubes_qty; }
        });
    return fewest_cubes;
}

fn main() {
    let input = read_file("inputs/in_day2.txt");

    let game_lines: Vec<&str> = input.lines().collect();

    let _games: Vec<Game> = game_lines.iter()
        .map(|line| parse_game(line))
        .collect();

    let _possible_games: Vec<Game> = _games.iter()
        .cloned()
        .filter(|game| validate_game(game))
        .collect();

    let _possible_games_id_sum: u32 = _possible_games
        .iter()
        .map(|_game| {
            _game.game_id
        })
        .sum();
    println!(
        "PART1:\n   Sum of game id of possible games is {}",
        _possible_games_id_sum
    );

    ///////
    /*        PART2       */

    let fewest_cubes_for_moves: Vec<Move> = _games.iter()
        .map(|_game| find_least_cubes_for_game(&_game.moves))
        .collect();

    let power_of_set_of_cubes: u32 = fewest_cubes_for_moves
        .iter()
        .map(|_move| {
            _move.red_cubes_qty * _move.green_cubes_qty * _move.blue_cubes_qty
        })
        .sum();
    println!(
        "PART2:\n   Sum of power of set of cubes is {}",
        power_of_set_of_cubes
    );
}
